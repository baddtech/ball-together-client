(function (document) {
    'use strict';

    var app = document.querySelector('#app');

    app.baseUrl = '/';

    app.goLanding = function () {
        app.$.pages.selected = 0;
        app.$.homeView.closeDrawer();
    };

    app.goLogin = function () {
        app.$.pages.selected = 1;
        app.$.homeView.closeDrawer();
    };

    app.goSignup = function () {
        app.$.pages.selected = 2;
        app.$.homeView.closeDrawer();
    };

    app.goHome = function () {
        app.$.pages.selected = 3;
        app.$.homeView.closeDrawer();
    };

    app.showSplashScreen = function () {
        navigator.splashscreen.show();
    };

    app.hideSplashScreen = function () {
        navigator.splashscreen.hide();
    };

    var appReady = false;

    app.addEventListener('dom-change', function () {
        console.log('App ready');
        appReady = true;
    });

    window.addEventListener('WebComponentsReady', function () {

    });

    document.addEventListener('deviceready', function () {
        console.log('Cordova ready');
        (function loop() {
            if (appReady) {
                setTimeout(app.hideSplashScreen, 250);
            } else {
                setTimeout(loop, 4);
            }
        })();
    }, false);

})(document);
